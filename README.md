# TempWatcher

This very simple app relies on a raspberry-pi connected to a temperature and humidity sensor (dht 11) to allow a constant monitoring of a room temperature.
By default, it takes the temperature every 15 minutes and send batches of 48 measures to a remote Google SpreadSheet using its Google API.
This project was originally made for ensuring that the heating of a house was working without having to go there in person.
If one do not have/want to setup a bluetooth communication with a raspberry-pi, the app also takes some internal temperature measure from the device that reflects the variation in room temperature when the device not used.
## Installation

Start by cloning this project in your favorite IDE (I use Android Studio)
If you want the app to send its results to Google SpreadSheets, you'll have to register the app at (https://console.developers.google.com/) using the package name and the SHA-1 signature that you'll have to generate.
Then at the first launch of the application, you'll be redirected to authenticate with Google.

## Usage

After the first run of the application, a workplace will be initialized. 
A folder named TempWatcher should be created in the local storage with an empty cache file, a newly created report.csv, and a configuration file.
You can edit the configuration file to set the time between the measure taking and the number of measures to accumulate before sending to Google SpreadSheet. There is also a parameter that if set to True will save the logs on device. Allowing you to debug without having to be connected to a computer.

NOTE:
If you want to rely on the internal temperature measures from the device, you will most probably have to edit the code to target the right path on which various temperatures of the phone are stored.
Not all phones will save their temperature files in the same directory. The paths that I used are the ones working for a Huawei Y6 (2015).

## Contributing
Pull requests are welcome. This is a first public project for me so comments are welcome as well. Though I'm not sure that the application will be evolving much more in the future as this was developed for a specific need of mine.


## License
