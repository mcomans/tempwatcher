#!/usr/bin/python
import sys
import Adafruit_DHT
import glob
from bluetooth import *
from datetime import datetime

#while True:
def read_temp():
    humidity, temperature = Adafruit_DHT.read_retry(11, 4)
    while float(humidity) > 100.0: #avoid returning a corrupted measure
        humidity, temperature = Adafruit_DHT.read_retry(11, 4)
    print("Temp :",temperature, "humidity :",humidity,"%")
    return ""+str(temperature)+";"+ str(humidity)+"!"
    

server_sock = BluetoothSocket(RFCOMM)
server_sock.bind(("",PORT_ANY))
server_sock.listen(1)

port = server_sock.getsockname()[1]

uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"

advertise_service(server_sock, "TemPIserver",
                  service_id = uuid,
                  service_classes = [uuid, SERIAL_PORT_CLASS],
                  profiles = [SERIAL_PORT_PROFILE],
#                  protocols = [OBEX_UUID]
                  )

if int(port) > 1: # abort if socket on port 1 is already listening to TempWatcher
    server_sock.close()
    exit(0)
    sys.exit("BT port 1 busy, abort.");


while True:
    time = datetime.now().strftime("%d-%m-%y:%H:%M:%S")
    print(time,"waiting for connection on port",port)
    client_sock, client_info = server_sock.accept()
    print("accepted connection with", client_info)
    try:
        data= client_sock.recv(1024)
        if len(data) ==0:break
        print("received:",data)
        data = read_temp()
        client_sock.send(data)
        print("sending",data)
        
    except IOError:
        print("IOError")
        pass

    except KeyboardInterrupt:
        print("disconnection")
        client_sock.close()
        server_sock.close()
        print("bye")
        break