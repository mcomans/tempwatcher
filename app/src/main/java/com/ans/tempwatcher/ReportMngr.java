package com.ans.tempwatcher;

import android.os.Environment;
import android.util.Log;

import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;

import com.google.api.services.drive.DriveScopes;
import com.google.api.services.sheets.v4.model.ValueRange;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Helper class to edit the CSV report stored in local
 * and to keep in cache the fileID of the google sheet
 * Also allows a recovery from local.
 */
public class ReportMngr {
    private final String TAG = "ReportManager";
    private final String REPORT_FILE = "report.csv";
    private final String CACHE_FILE = "/cache.txt";
    private FileOutputStream report_output_stream;
    private static String root = Environment.getExternalStorageDirectory().toString();
    private static final String filePath = root +"/TempWatcher";
    private File dir;
    public ReportMngr(){
        // Open the right directory to access the cache and local report
        dir = new File(filePath);
        if (!dir.exists()) {
            Log.w(TAG, "Creating new folder");
            dir.mkdirs();
        }
        openReportStream();

    }

    /**
     * If a Google SpreadSheet was previously created, we should have stored its fileID
     *
     * @return
     */
    public String retrieveIdFromCache(){
        String id = "no_id";
        try {
            File cache = new File(filePath+CACHE_FILE);
            if (cache.createNewFile()){
                Log.w(TAG, "Initializing a new cache file.");
//                System.out.println("New cache file created");
            }
            Scanner myReader = new Scanner(cache);
            if(myReader.hasNextLine()) {
                String data = myReader.nextLine();
                id = data;
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Cache file not found!\n"+e);
//            e.printStackTrace();
        } catch (IOException e){
            Log.e(TAG,"Something went wrong while reading cache!\n"+e);
        }
        Log.i(TAG, "SpreadSheet id stored in cache"+id);
        return id;
    }
    public void writeIdToCache(String ggFileId){
        try {
            FileWriter myWriter = new FileWriter(filePath+CACHE_FILE);
            myWriter.write(ggFileId);
            myWriter.close();
            Log.i(TAG, "New fileID stored in cache: "+ggFileId);
        } catch (IOException e){
            Log.i(TAG,"Something went wrong while writing in cache\n"+e);
        }
    }
    private void openReportStream(){
        // Create or open the report in local
        File report = new File(dir, REPORT_FILE);
        try {
            boolean blank = report.createNewFile(); // if file already exists will do nothing
            FileOutputStream oFile = new FileOutputStream(report, true);
            if(blank){
                Log.i(TAG, "New report file created");
                String header = "Date;Time;TempC;Humidity;pa_therm0;tsens_tz_sensor0;battery\n";
                oFile.write(header.getBytes());
            }
            else{
                Log.i(TAG,"Retrieving existing report file");
            }
            report_output_stream = oFile;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public Boolean writeEntryInReport(String date, String time, String tempc, String humidity, String pa, String ts, String bat){
        if(report_output_stream != null){
            String entry = date+";"+time+";"+tempc+";"+humidity+";"+pa+";"+ts+";"+bat+"\n";
            try {
                report_output_stream.write(entry.getBytes());
            } catch (IOException e) {
                return false;
            }
            return true;
        }
        return false;
    }
    public ValueRange recoverFromLocal(){
        ValueRange recovery = new ValueRange();
        try {
            File report = new File(filePath+"/"+REPORT_FILE);
            Scanner myReader = new Scanner(report);
            List<List<Object>> list = new ArrayList<>();
            while(myReader.hasNextLine()) {
                String line = myReader.nextLine();
                String[] data = line.split(";");
                List<Object> row = new ArrayList<>(Arrays.asList(data));
                list.add(row);
            }
            recovery.setValues(list);
            myReader.close();

        } catch (FileNotFoundException e) {
            Log.e(TAG, "Report file not found to perform a recovery!");
            e.printStackTrace();
        }

        return recovery;
    }

}
