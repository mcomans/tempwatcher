package com.ans.tempwatcher;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.AppendValuesResponse;
import com.google.api.services.sheets.v4.model.Spreadsheet;
import com.google.api.services.sheets.v4.model.SpreadsheetProperties;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.google.gson.Gson;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;


public class MainActivity extends Activity{

    private static final String TAG = "MainActivity";
    private final String APP_FOLDER = Environment.getExternalStorageDirectory().toString()+"/TempWatcher";

    // gg sign-in option
    private static final int REQUEST_CODE_SIGN_IN = 1;
    private static final int REQUEST_CODE_OPEN_DOCUMENT = 2;

    // gg sheets insert options
    private final String VALUE_INPUT_OPTION = "RAW";
    private final String INSERT_DATA_OPTION = "INSERT_ROWS";
    private final String RANGE = "A:G";


    // date format conversion
    private final SimpleDateFormat today_to_date = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
    private final SimpleDateFormat today_to_time = new SimpleDateFormat("HH:mm", Locale.getDefault());
//    private Button getTemp;
//    TempCallBack tcb = TempCallBack.getInstance();

    // Temperatures retrieved from device
    private String battery_temp;
    private String pa_therm0_temp;
    private String tsens_tz_sensor0_temp;

    // Bluetooth
    private final byte delimiter = 33; // '!' marks the end of a bluetooth message
    private final String SERVER_BT_NAME = "raspberrypi";
    private BluetoothSocket mmSocket;
    private BluetoothDevice mmDevice = null;
    private int readBufferPosition = 0;
    private String last_temp_received;
    private String last_humid_received;

    //
    private long MEASURE_TAKING_INTERVAL;// = AlarmManager.INTERVAL_FIFTEEN_MINUTES/15;
    private int COUNT_TRIGGER_UPLOAD;// = 10;
    private boolean logsEnabled;
    // Google Sheets API
    private Handler handler;
//    private Drive googleDriveService;
    private Sheets googleSheetService;
    private String spread_sheet_id;

    // general global variables
    ReportMngr report_manager;
    List<List<Object>> list_accumulated_measures;
    private TextView tv_last_uptd;
    private TextView tv_cumul;
    private TextView tv_last_fetch;
    private TextView tv_temperature;
    private TextView tv_humidity;
    private TextView tv_battery;
    private TextView tv_pa_therm0;
    private TextView tv_tsens_tz_sensor0;
    private BroadcastReceiver br;
    private Date last_update;
    private Date last_fetch;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // start by reading the configuration file
        super.onCreate(savedInstanceState);
        readConfigurationFile();
        Log.d(TAG,"Starting with conf.: Time interval:"+MEASURE_TAKING_INTERVAL/60000+"; Count uploading:"+COUNT_TRIGGER_UPLOAD+"; Logs enabled"+logsEnabled);
        // redirect the logs to be saved on the device if instructed to
        if(logsEnabled){
            java.io.File appDirectory = new java.io.File(APP_FOLDER);
            java.io.File logDirectory = new java.io.File(APP_FOLDER + "/logs");
            java.io.File logFile = new java.io.File(logDirectory, "logcat_" + System.currentTimeMillis() + ".txt");
            // create app folder
            if ( !appDirectory.exists() ) {
                appDirectory.mkdir();
            }
            // create log folder
            if ( !logDirectory.exists() ) {
                logDirectory.mkdir();
            }
            // clear the previous logcat and then write the new one to the file
            try {
                Process process = Runtime.getRuntime().exec("logcat -c");
                process = Runtime.getRuntime().exec("logcat -f " + logFile);
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        }
        // start the UI
        Log.d(TAG,"start UI");
        setContentView(R.layout.activity_main);
        initGlobalVariables();

        // initialize the handler for the broadcastReceiver
        handler = new Handler();
        HandlerThread handlerThread = new HandlerThread("ht");
        handlerThread.start();
        Looper looper = handlerThread.getLooper();
        Handler handler = new Handler(looper);
        initBroadcastReceiver();
        registerReceiver(br, new IntentFilter("com.ans.checkAndUpload"), null, handler);

        // Get/ask for the user's Google credentials
        Log.d(TAG,"request sign-in");
        requestSignIn();

        // Pair and connect to the raspberry-pi via bluetooth
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(!mBluetoothAdapter.isEnabled())
        {
            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetooth, 0);
        }
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if(pairedDevices.size() > 0){
            for(BluetoothDevice device : pairedDevices){
                if(device.getName().equals(SERVER_BT_NAME)){
                    Log.i(TAG, "found "+device.getName());
                    mmDevice = device;
                    break;
                }

            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        switch (requestCode) {
            case REQUEST_CODE_SIGN_IN:
                if (resultCode == Activity.RESULT_OK && resultData != null) {
                    Log.i(TAG,"Initialization of Google service");
                    handleSignInResult(resultData);
                }
                break;

            case REQUEST_CODE_OPEN_DOCUMENT:
                if (resultCode == Activity.RESULT_OK && resultData != null) {
                    Log.w(TAG,"Request to open document not yet implemented");
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, resultData);
    }

    /**
     * Retrieve TextViews and initialize variables for the report
     */
    public void initGlobalVariables(){
        tv_last_uptd = (TextView) findViewById(R.id.last_uptd);
        tv_cumul = (TextView) findViewById(R.id.cumul);
        tv_last_fetch = (TextView) findViewById(R.id.last_fetch);
        tv_temperature = (TextView) findViewById(R.id.temp);
        tv_humidity = (TextView) findViewById(R.id.humidity);
        tv_battery = (TextView) findViewById(R.id.battery_temp);
        tv_pa_therm0 = (TextView) findViewById(R.id.pa_therm0_temp);
        tv_tsens_tz_sensor0 = (TextView) findViewById(R.id.tsens_tz_sensor0_temp);
        report_manager = new ReportMngr();
        spread_sheet_id = report_manager.retrieveIdFromCache();
        list_accumulated_measures = new ArrayList<>();
    }

    /**
     * Sets the behavior triggered every {MEASURE_TAKING_INTERVAL} :
     * Read the device internal temperatures and send a query via BT to the raspberry
     * then save the measures with a timestamp in memory.
     * If we have reached {COUNT_TRIGGER_UPLOAD}, upload them all to the Google SpreadSheet
     */
    public void initBroadcastReceiver(){
        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context c, Intent i) {
                last_fetch = Calendar.getInstance().getTime();
                Log.i(TAG, "Current time => " + last_fetch);
                String formattedDate = today_to_date.format(last_fetch); //dd-MM-yyy
                String formattedTime = today_to_time.format(last_fetch); //HH-mm
                readDeviceTemps();
                Thread contactRaspberry = new Thread(new workerThread("AskingForTemp"));
                contactRaspberry.start();
                try {
                    contactRaspberry.join(); // wait until we get the result
                    // save the measures in local
                    report_manager.writeEntryInReport(formattedDate,
                            formattedTime, last_temp_received, last_humid_received, pa_therm0_temp, tsens_tz_sensor0_temp, battery_temp);
                    // store in memory the last COUNT_TRIGGER_UPLOAD measures before uploading
                    list_accumulated_measures.add(Arrays.asList(formattedDate,
                            formattedTime, last_temp_received, last_humid_received, pa_therm0_temp, tsens_tz_sensor0_temp, battery_temp));
                    Log.i(TAG, "IN MEMORY: "+list_accumulated_measures.size());
                    if(list_accumulated_measures.size() >= COUNT_TRIGGER_UPLOAD){
                        //upload
                        ValueRange measuresToUpload = new ValueRange();
                        measuresToUpload.setValues(list_accumulated_measures);
                        appendMultipleRows(measuresToUpload);
                    }
                } catch (InterruptedException e) {
                    Log.e(TAG,"Something went wrong while fetching temp\n"+e);
                }
                // update the info shown in the UI
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(last_update != null){
                            String t0 = new SimpleDateFormat("HH:mm (dd-MM)", Locale.getDefault()).format(last_update);
                            tv_last_uptd.setText(t0);
                        }
                        if(last_fetch != null){
                            String t1 = new SimpleDateFormat("HH:mm (dd-MM)", Locale.getDefault()).format(last_fetch);
                            tv_last_fetch.setText(t1);
                        }
                        String t2 = ""+list_accumulated_measures.size();
                        tv_cumul.setText(t2);
                        tv_temperature.setText(last_temp_received);
                        tv_humidity.setText(last_humid_received);
                        String t3 = battery_temp+" °C";
                        String t4 = pa_therm0_temp+ " °C";
                        String t5 = tsens_tz_sensor0_temp+ " °C";
                        tv_battery.setText(t3);
                        tv_pa_therm0.setText(t4);
                        tv_tsens_tz_sensor0.setText(t5);
                    }
                });
            }
        };
    }

    /**
     * Setup a recurring alarm every {MEASURE_TAKING_INTERVAL}
     */
    public void scheduleAlarm() {
        AlarmManager mgr=(AlarmManager) getSystemService(Context.ALARM_SERVICE);
        PendingIntent pi = PendingIntent.getBroadcast( this, 0, new Intent("com.ans.checkAndUpload"),0);
        Log.i(TAG,"setting alarm every "+MEASURE_TAKING_INTERVAL/60000+" min");

        mgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), MEASURE_TAKING_INTERVAL, pi);
    }

    /**
     * Starts a sign-in activity using {@link #REQUEST_CODE_SIGN_IN}.
     */
    private void requestSignIn() {
        Log.i(TAG, "Requesting sign-in");

        GoogleSignInOptions signInOptions =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .requestScopes(new Scope(SheetsScopes.SPREADSHEETS))
                        .build();
        GoogleSignInClient client = GoogleSignIn.getClient(this, signInOptions);
        Log.i(TAG, "found client");

        // The result of the sign-in Intent is handled in onActivityResult.
        startActivityForResult(client.getSignInIntent(), REQUEST_CODE_SIGN_IN);
    }

    /**
     * Handles the {@code result} of a completed sign-in activity initiated from {@link
     * #requestSignIn()}.
     */
    private void handleSignInResult(Intent result) {
        GoogleSignIn.getSignedInAccountFromIntent(result)
                .addOnSuccessListener(googleAccount -> {
                    Log.i(TAG,"Signed in as " + googleAccount.getEmail());

                    // Use the authenticated account to sign in to Google Sheets API
                    GoogleAccountCredential sheetsCredential =
                            GoogleAccountCredential.usingOAuth2(this, Collections.singleton(SheetsScopes.SPREADSHEETS));
                    sheetsCredential.setSelectedAccount(googleAccount.getAccount());

                    googleSheetService = new Sheets.Builder(
                            AndroidHttp.newCompatibleTransport(),
                            new GsonFactory(),
                            sheetsCredential)
                            .setApplicationName("GoogleSheets API")
                            .build();

                    /** At this point we are connected and can start the recurring timer **/
                    Log.i(TAG,"Starting the recurring timer");
                    scheduleAlarm();
                })
                .addOnFailureListener(exception -> Log.e(TAG,"Unable to sign in.\n"+ exception));
    }

    /**
     * Helper to append a new line ranging from {RANGE} into the Google SpreadSheet
     * @param date
     * @param time
     * @param tempc
     * @param humidity
     * @param pa
     * @param cpu
     * @param bat
     */
    private void appendSingleRow(String date, String time, String tempc, String humidity, String pa, String cpu, String bat){
        ValueRange requestBodyAppend = new ValueRange();
        requestBodyAppend.setValues(
                Arrays.asList(
                        Arrays.asList(date, time, tempc, humidity, pa, cpu, bat)));
        appendMultipleRows(requestBodyAppend);
    }

    /**
     * Helper to append a multiple lines ranging from {RANGE} into the Google SpreadSheet
     * @param request_body: list of 'tuples' of measures and time
     */
    private void appendMultipleRows(ValueRange request_body){
        try {
            Sheets.Spreadsheets.Values.Append requestAppend =
                    googleSheetService.spreadsheets().values().append(spread_sheet_id, RANGE, request_body);
            requestAppend.setValueInputOption(VALUE_INPUT_OPTION);
            requestAppend.setInsertDataOption(INSERT_DATA_OPTION);

            AppendValuesResponse responseAppend = requestAppend.execute();
            Log.i(TAG,"New measures inserted in" + responseAppend.getUpdates().getUpdatedRange());
            list_accumulated_measures = new ArrayList<>();
            last_update = Calendar.getInstance().getTime();
        }
        catch (GoogleJsonResponseException ge){
            if(ge.getDetails().getCode() == 404){
                Log.w(TAG,"Appending on a non-existing file! Creating a new one...");
                //File was not previously created
                createNewSpreadSheet();
                appendMultipleRows(request_body);
            }
        }
        catch (IOException e){
            Log.e(TAG, "Unexpected error while appending rows :\n"+e);
        }


    }

    /**
     * Helper to create a new Google SpreadSheet with the name "report.csv"
     * Save the newly created file ID in the cache
     */
    private void createNewSpreadSheet() {
        Spreadsheet requestBody = new Spreadsheet().setProperties(new SpreadsheetProperties()
                .setTitle("report.csv"));

        try {
            Sheets.Spreadsheets.Create request = googleSheetService.spreadsheets().create(requestBody);
            Spreadsheet response = request.execute();
            spread_sheet_id = response.getSpreadsheetId();
            report_manager.writeIdToCache(spread_sheet_id); // save the file id to cache
            Log.i(TAG, "New SpreadSheet created at "+response.getSpreadsheetUrl());
            appendSingleRow("Date", "Time", "TempC", "Humidity","pa_therm0","tsens_tz_sensor0","battery");

        } catch (IOException e) {
            Log.e(TAG,"Something went wrong while creating a new spreadsheet!\n" + e);
        }
    }

    /**
     * Thread to handle Bluetooth communication with the raspberry-pi
     * Start by sending a greeting message and then listen for a response containing
     * measures of temperature and humidity
     */
    final class workerThread implements Runnable {
        private String btMsg;

        public workerThread(String msg) {
            btMsg = msg;
        }
        public void run(){
            boolean host_is_alive = sendBtMsg(btMsg); // sending "hello" to asses the connection
            if(!host_is_alive) return;
            while(!Thread.currentThread().isInterrupted()){
                int bytesAvailable;
                boolean workDone = false;
                try{
                    final InputStream mmInputStream;
                    mmInputStream = mmSocket.getInputStream();
                    bytesAvailable = mmInputStream.available();
                    if(bytesAvailable > 0){
                        byte[] packetBytes = new byte[bytesAvailable];
                        Log.i(TAG, "Receiving Bluetooth Message...");
                        byte[] readBuffer = new byte[1024];
                        mmInputStream.read(packetBytes);
                        for(int i=0;i<bytesAvailable;i++){
                            byte b = packetBytes[i];
                            if(b == delimiter){
                                byte[] encodedBytes = new byte[readBufferPosition];
                                System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                final String data = new String(encodedBytes, "US-ASCII");
                                readBufferPosition = 0;
                                Log.i(TAG, "Data Received: "+data);
                                String[] temp_and_humid = data.split(";");
                                last_temp_received = temp_and_humid[0];
                                last_humid_received = temp_and_humid[1];
                                workDone = true;
                                break;
                            }
                            else{
                                readBuffer[readBufferPosition++] = b;
                            }
                        }
                        if (workDone){
                            mmSocket.close();
                            break;
                        }
                    }
                }
                catch (IOException e) {
                    Log.e(TAG, "Something went wrong while processing Bluetooth message! "+e);
                }
            }
        }
    }

    /**
     *
     * @param msg2send
     * @return True if the host is alive, False otherwise
     */
    public boolean sendBtMsg(String msg2send){
        // Standard SerialPortService ID | corresponds to the address used in the raspberry code
        UUID uuid = UUID.fromString("94f39d29-7d6d-437d-973b-fba39e49d4ee");
        try {

            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
            if (!mmSocket.isConnected()){
                mmSocket.connect();
            }

            OutputStream mmOutputStream = mmSocket.getOutputStream();
            mmOutputStream.write(msg2send.getBytes());
            return true;

        } catch (IOException e) {
            // Could not establish a connection
            Log.e(TAG, "Something went wrong while sending Bluetooth message! "+e);
            last_humid_received = "na";
            last_temp_received = "na";
            return false;
        }
    }

    /**
     * Retrieving files monitoring temperatures in the device
     * PATHS ARE DEVICE DEPENDENT! Works for a Huawei Y6 2015
     * One might need to check the commented paths for other phones
     * or to perform a search using adb shell to look for 'temp' files.
     */
    private void readDeviceTemps(){
        String[] possiblePathList =
                {
                        "/sys/devices/virtual/hwmon/hwmon1/temp1_input", // power amplifier °C
                        "/sys/devices/virtual/hwmon/hwmon4/temp1_input", // (?) CPU °C
                        "/sys/devices/virtual/hwmon/hwmon11/temp1_input" // battery °C *1000
                };
//        "/sys/devices/system/cpu/cpu0/cpufreq/cpu_temp"
//        "/sys/devices/system/cpu/cpu0/cpufreq/FakeShmoo_cpu_temp",
//        "/sys/class/thermal/thermal_zone1/temp",
//        "/sys/class/i2c-adapter/i2c-4/4-004c/temperature",
//        "/sys/devices/platform/tegra-i2c.3/i2c-4/4-004c/temperature",
//        "/sys/devices/platform/omap/omap_temp_sensor.0/temperature",
//        "/sys/devices/platform/tegra_tmon/temp1_input",
//        "/sys/kernel/debug/tegra_thermal/temp_tj",
//        "/sys/devices/platform/s5p-tmu/temperature",
//        "/sys/class/thermal/thermal_zone0/temp",
//        "/sys/devices/virtual/thermal/thermal_zone0/temp",
//        "/sys/class/hwmon/hwmon0/device/temp1_input",
//        "/sys/devices/virtual/thermal/thermal_zone1/temp",
//        "/sys/devices/platform/s5p-tmu/curr_temp",
//        "/sys/htc/cpu_temp",
//        "/sys/devices/platform/tegra-i2c.3/i2c-4/4-004c/ext_temperature",
//        "/sys/devices/platform/tegra-tsensor/tsensor_temperature",

        RandomAccessFile reader = null;
        try {
            reader = new RandomAccessFile(possiblePathList[0], "r");
            pa_therm0_temp = reader.readLine();

            reader = new RandomAccessFile(possiblePathList[1], "r");
            tsens_tz_sensor0_temp = reader.readLine();

            reader = new RandomAccessFile(possiblePathList[2], "r");
            double bat = Integer.parseInt(reader.readLine()) / 1000.0;
            battery_temp = Double.toString(bat);

            Log.d(TAG, "found:"+pa_therm0_temp+" - "+tsens_tz_sensor0_temp+" - "+battery_temp+"\n");
        } catch (FileNotFoundException e) {
            Log.w(TAG, "can't find a file");
        } catch (IOException e) {
            Log.w(TAG, "Failed reading a file");
        }

    }

    /**
     * A configuration file object to define the values of
     * trigger_interval :      time between measure taking (in minutes)
     * num_records_uploading : number of measure to store before saving them in Google SpreadSheet
     * enable_logs :           if True, will save the log output in a txt file on device (debug)
     */
    private static class Conf{
        public int trigger_interval;
        public int num_records_uploading;
        public boolean enable_logs;
        public Conf(int interval, int count, boolean log){
            this.trigger_interval = interval;
            this.num_records_uploading = count;
            this.enable_logs = log;
        }

        @Override
        public String toString() {
            return "Conf{" +
                    "trigger_interval=" + trigger_interval +
                    ", num_records_uploading=" + num_records_uploading +
                    ", enable_logs=" + enable_logs +
                    '}';
        }
    }

    /**
     * Read the json configuration stored on device
     * Create a new on with default values if none is found.
     */
    private void readConfigurationFile(){
        Gson gson = new Gson();
        Conf configuration;

        try (Reader reader = new FileReader(APP_FOLDER+"/configuration.txt")) {
            // Convert JSON File to Java Object
            configuration = gson.fromJson(reader, Conf.class);
            Log.i(TAG, "configuration file found:\n"+configuration);

        } catch (IOException e) {
            Log.e(TAG, "Cannot open or read configuration file!");
            configuration = new Conf(15, 48, false); // default configuration
            try (FileWriter writer = new FileWriter(APP_FOLDER+"/configuration.txt")) {
                gson.toJson(configuration, writer);
                Log.i(TAG, "New configuration file created");
            } catch (IOException e2) {
                Log.e(TAG,"Could not create a default configuration file!\n"+e2);
            }
        }
        MEASURE_TAKING_INTERVAL = configuration.trigger_interval * 60 * 1000;
        COUNT_TRIGGER_UPLOAD = configuration.num_records_uploading;
        logsEnabled = configuration.enable_logs;
    }
}